# 05-tour-of-heroes
We will deploy an application called `Tour of Heroes`. Tour of heroes is a Single Page Application (SPA) and is 
part of the [Angular tutorial](https://angular.io/tutorial/toh-pt1).

Tour of heroes will be deployed by means of CloudFront, that will be using a Deployment Bucket as the origin.
We will use API Gateway as the API, that will be fronted by CloudFront. The API will be made available from the `/api` 
resource. For the stateful backend, we will be using [DynamoDB](https://aws.amazon.com/dynamodb/), a serverless database. 

## Assignment
Do the following:

- read `templates/api-gateway.yaml`, what objects are deployed?
- read `templates/cloudfront.yaml`, what objects are deployed?
- read [DynamoDB](https://aws.amazon.com/dynamodb/)
- read [DynamoDB API Operations](https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_Operations.html)
- deploy the stack
- Note: Deploying the stack can take up to 15 minutes!

## Login to the AWS Console

- go to [https://console.aws.amazon.com](https://console.aws.amazon.com)
- switch to the Admin role
- switch to the region `N. Virginia (us-east-1)`
- go to CloudFront
- Click on the distribution and view the properties
- Go to DynamoDB
- View the heroes table
- Go to API Gateway
- View the methods and integrations

## Add items to the heroes table
Add two items to the heroes table:

```text
aws dynamodb put-item --table-name heroes --profile xtreme-deploy --region us-east-1 --item '{ "id": {"N": "1"},"name": {"S": "superman"}}'
aws dynamodb put-item --table-name heroes --profile xtreme-deploy --region us-east-1 --item '{ "id": {"N": "2"},"name": {"S": "batman"}}'
```

Get an item:

```text
aws dynamodb get-item --table-name heroes --profile xtreme-deploy --region us-east-1 --key '{"id": {"N": "1"}}'
```

View all items:

```text
aws dynamodb scan --table-name heroes --profile xtreme-deploy --region us-east-1
```

Search for items in the table:

```text
aws dynamodb scan --table-name heroes --profile xtreme-deploy --region us-east-1 \
    --scan-filter '{
        "name":{
            "AttributeValueList":[ {"S":"superman"} ],
            "ComparisonOperator": "CONTAINS"
        }
    }'
```

Update an item:

```text
aws dynamodb update-item --table-name heroes --profile xtreme-deploy --region us-east-1 \
    --key '{"id":{"N":"1"}}' \
    --update-expression "SET #n = :p" \
    --expression-attribute-values '{":p": {"S":"fooman"}}' \
    --expression-attribute-names '{"#n":"name"}'        
```

Delete an item:

```text
aws dynamodb delete-item --table-name heroes --profile xtreme-deploy --region us-east-1 --key '{"id": {"N": "1"}}'
```

Add some more items to the table!

## Questions
CloudFront:
- What is the value of `Distribution Id`?
- What is the bucket name of the CloudFront Distribution?

API Gateway:
- Take a look at `swagger/swagger.yaml`, what can you tell about the API definition?
- Take a look at `swagger/aws-extensions.yaml`, what can you tell about the integrations?
- 

DynamoDB:

## Accessing CloudFront
When the distribution is finished, which can take up to 15 minutes, access the distribution by using the domain name in
your browser. There should be no content.

## Distributing Tour of Heroes
- Copy the contents of the `dist` directory to the deployment bucket, hint: use `aws s3 sync`
- Access the CloudFront distribution. 
- It can take some time before the content is available and CloudFront can respond with HTTP 307, but after some minutes
the content should become available.

## View the Tour of Heroes Application
Access the CloudFront distribution and use the user interface to add/remove and query heroes.

## Cleanup

- Delete the stack (can take up to 15 minutes)
