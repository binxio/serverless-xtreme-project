# 01-bucket
A simple bucket

## Assignment
Do the following:

- read `templates/bucket.yaml`
- read [AWS::S3::Bucket](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-s3-bucket.html)
- read `aws s3 help`

## Create the bucket
Type: 

```
make create
```

## Get the name of the bucket
Type: 

```
make describe
```

## All awscli S3 commands:

- ls: list all files
- cp: copy files from local to the cloud
- rm: remove a file
- mb: make a bucket
- rb: remove a bucket
- mv: move a file
- sync: copy all files and dirs to the bucket 

## List all files in the bucket
Type: 

```
aws s3 ls s3://<bucket name> --profile xtreme-deploy
```

## Copy a file to the bucket

Type:

```
aws s3 cp readme.md s3://<bucket name> --profile xtreme-deploy
```

## sync all files with the bucket

```text
aws s3 sync . s3://<bucket name> --profile xtreme-deploy
```

## Assignment
Delete all files of the bucket recursively

## Remove the bucket
Type:

```text
make delete
```