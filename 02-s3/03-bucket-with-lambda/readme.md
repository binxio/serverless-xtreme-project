## 03-bucket-with-lambda
A bucket with lambda as event notification handler

## Assignment
Do the following:

- read `templates/bucket-with-lambda.yaml`
- read [NotificationConfiguration](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-s3-bucket-notificationconfig.html)
- read [LambdaConfiguration](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-s3-bucket-notificationconfig-lambdaconfig.html)
- type: `make merge-lambda`
- deploy the stack

## Study the code
Study the lambda code in `templates/notification-handler.yaml` and use the [boto3 website](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html)
to lookup the API calls that the lambda is making. What is the lambda doing?

## Try out the lambda
Copy a file to the input bucket, and list the contents of the output bucket. What did just happen?

## Get the logs of the lambda
Get the logs of the lambda:

Get the `FunctionName` of the NotificationEventHandler with the command:

```text
aws lambda list-functions --profile xtreme-deploy --region eu-west-1
```

To get the log streams names of the lambda, replace `<FunctionName>`: 

```text
aws logs describe-log-streams --log-group-name '/aws/lambda/<FunctionName>' --query logStreams[*].logStreamName --profile xtreme-deploy --region eu-west-1
``` 

To get the log events, replace `<FunctionName>` and `<LogStreamName>`:

```text
aws logs get-log-events --log-group-name '/aws/lambda/<FunctionName>' --log-stream-name '<LogStreamName>' --profile xtreme-deploy --region eu-west-1
```

## Cleanup

- Delete all files from the bucket
- Delete the stack