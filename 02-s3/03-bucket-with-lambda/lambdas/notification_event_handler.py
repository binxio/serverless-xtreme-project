import boto3


def lambda_handler(event, context):
    output_bucket_name = '${OutputBucketName}'
    s3 = boto3.resource('s3')
    for record in event['Records']:
        src_bucket = record['s3']['bucket']['name']
        src_key = record['s3']['object']['key']
        print(f'Copying {src_key} from {src_bucket} to {output_bucket_name}')
        copy_source = {
            'Bucket': src_bucket,
            'Key': src_key
        }
        bucket = s3.Bucket(output_bucket_name)
        bucket.copy(copy_source, src_key)