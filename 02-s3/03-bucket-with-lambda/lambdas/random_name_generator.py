import string
import random
import cfnresponse


def lambda_handler(event, context):
    if event['RequestType'] == 'Create':
        event['PhysicalResourceId'] = ''.join(random.choice(string.ascii_lowercase) for _ in range(16))
    cfnresponse.send(event, context, cfnresponse.SUCCESS, {}, event['PhysicalResourceId'])
