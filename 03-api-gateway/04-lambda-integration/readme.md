# 04-lambda-integration
We will create a simple rest api with [lambda proxy integrations](https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html).  

## Assignment
Do the following:

- read `swagger/swagger.yaml`
- read `swagger/aws-extensions`
- read `templates/api-gateway.yaml`, what object is deployed?
- read [API Gateway - Lambda Proxy Integrations](https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html)
- read [API Gateway - Lambda Proxy Response Format](https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-output-format)
- read [AWS::Lambda::Function](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-lambda-function.html)
- type: `make merge-lambda`
- type: `make merge-swagger`
- read `templates/api-gateway.yaml`, what has happened?
- deploy the stack

## Get a list of deployed APIs

```text
$ aws apigateway get-rest-apis --profile xtreme-deploy --region eu-west-1
```

What is the `id` of the deployed API? 
What is the `name` of the API?
What is the `version` of the API?

## Accessing the API
Use the Invoke URL and access the `Tour of Heroes` API.

## Login to the AWS Console

- go to [https://console.aws.amazon.com](https://console.aws.amazon.com)
- switch to the Admin role
- go to API Gateway
- View the 'Tour of Heroes' API, and see all the method requests
- Note that there is a 'prod' stage. 
- Note the Invoke URL: https://<api-id>.execute-api.<region>.amazonaws.com/<stage>
- go to Lambda
- Search for the Notification Lambda
- View the user interface, you can edit the code
- Make some small changes to the code and click 'SAVE' at the top right to update the Lambda
- The update to the lambda is instantly!
- Click on monitoring, view the available metrics
- Click on 'View logs in Cloudwatch'
- View the logs of the lambda

## Questions
- What does the 'lambda proxy integration' do?
- What is the response format of the proxy lambda
- What does the lambda do?
- How does the lambda handle the event?
- How does the proxy lambda respond?

## Assignment
- Make changes to the lambda and test the result

## Cleanup

- Delete the stack
