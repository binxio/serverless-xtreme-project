# 02-mock-integration
We will create a simple rest api with [mock integrations](https://docs.aws.amazon.com/apigateway/latest/developerguide/how-to-mock-integration.html).  

## Assignment
Do the following:

- read `swagger/swagger.yaml`
- read `swagger/aws-extensions`
- read `templates/api-gateway.yaml`, what object is deployed?
- read [API Gateway - Mock Integrations](https://docs.aws.amazon.com/apigateway/latest/developerguide/how-to-mock-integration.html)
- read [API Gateway Mapping Template Reference](https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-mapping-template-reference.html)
- read [Velocity Template Language Reference](http://velocity.apache.org/engine/1.7/user-guide.html)
- type: `make merge-swagger`
- read `templates/api-gateway.yaml`, what has happened?
- deploy the stack

## Get a list of deployed APIs

```text
$ aws apigateway get-rest-apis --profile xtreme-deploy --region eu-west-1
```

What is the `id` of the deployed API? 
What is the `name` of the API?
What is the `version` of the API?

## Login to the AWS Console

- go to [https://console.aws.amazon.com](https://console.aws.amazon.com)
- switch to the Admin role
- go to API Gateway
- View the 'Tour of Heroes' API, and see all the method requests
- Note that there is a 'prod' stage. 
- Note the Invoke URL: https://<api-id>.execute-api.<region>.amazonaws.com/<stage>

## Question
What does the 'mock integration' do?

## Accessing the API
Use the Invoke URL and access the `Tour of Heroes` API.  

## Cleanup

- Delete the stack
