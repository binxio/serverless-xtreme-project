# 03-http-integration
We will create a simple rest api with [http integrations](https://docs.aws.amazon.com/apigateway/latest/developerguide/setup-http-integrations.html).  

## Assignment
Do the following:

- read `swagger/swagger.yaml`
- read `swagger/aws-extensions`
- read `templates/api-gateway.yaml`, what object is deployed?
- read [API Gateway - HTTP Integrations](https://docs.aws.amazon.com/apigateway/latest/developerguide/setup-http-integrations.html)
- type: `make merge-swagger`
- read `templates/api-gateway.yaml`, what has happened?
- deploy the stack

## Get a list of deployed APIs

```text
$ aws apigateway get-rest-apis --profile xtreme-deploy --region eu-west-1
```

What is the `id` of the deployed API? 
What is the `name` of the API?
What is the `version` of the API?

## Login to the AWS Console

- go to [https://console.aws.amazon.com](https://console.aws.amazon.com)
- switch to the Admin role
- go to API Gateway
- View the 'Tour of Heroes' API, and see all the method requests
- Note that there is a 'prod' stage. 
- Note the Invoke URL: https://<api-id>.execute-api.<region>.amazonaws.com/<stage>

## Question
What does the 'http integration' do?

## Accessing the API
Use the Invoke URL and access the `Tour of Heroes` API.

## Cleanup

- Delete the stack
