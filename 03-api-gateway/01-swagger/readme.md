# 01-swagger
We will create a simple rest api without aws-integrations.  

## Assignment
Do the following:

- read `swagger/swagger.yaml`
- read `templates/api-gateway.yaml`, what object is deployed?
- read [OpenAPI v2.0 Specification - Basic Structure](https://swagger.io/docs/specification/2-0/basic-structure/)
- type: `make merge-swagger`
- read `templates/api-gateway.yaml`, what has happened?
- deploy the stack

## Get a list of deployed APIs

```text
$ aws apigateway get-rest-apis --profile xtreme-deploy --region eu-west-1
```

What is the `id` of the deployed API? 
What is the `name` of the API?
What is the `version` of the API?

## Login to the AWS Console

- go to [https://console.aws.amazon.com](https://console.aws.amazon.com)
- switch to the Admin role
- go to API Gateway
- View the 'Tour of Heroes' API, and see all the method requests
- Note that there are no integration requests defined.
- Note that there is no api defined at Stages, which means there is no API deployed

## Cleanup

- Delete the stack