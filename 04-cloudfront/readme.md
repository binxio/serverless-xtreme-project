# 04-CloudFront
We will deploy a bucket with sample that will be distributed globally using [AWS CloudFront](https://aws.amazon.com/cloudfront/), 
a Content Distribution Network (CDN) from AWS.

## Assignment
Do the following:

- read `templates/cloudfront.yaml`, what objects are deployed?
- read [AWS CloudFront Developer Documentation](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/Introduction.html)
- read [AWS::CloudFront::Distribution](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-cloudfront-distribution.html#aws-resource-cloudfront-distribution-syntax)
- read [Moving an Amazon S3 Bucket to a Different Region](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/DownloadDistS3AndCustomOrigins.html#move-s3-bucket-different-region)
- read [Cloudfront redirecting to S3 endpoint with 307 Temporary Redirect](https://stackoverflow.com/questions/46520566/cloudfront-redirecting-to-s3-endpoint-with-307-temporary-redirect)
- deploy the stack
- Note: Deploying the stack can take up to 15 minutes!

## Login to the AWS Console

- go to [https://console.aws.amazon.com](https://console.aws.amazon.com)
- switch to the Admin role
- switch to the region `N. Virginia (us-east-1)`
- go to CloudFront
- Click on the distribution and view the properties

## Questions
General:
- What is the value of `Distribution Id`?
- What is the value of `Price Class` and what does it mean?
- What is the value of `Distribution Status`?
- What is the value of `Domain Name` of the distribution?
- What is the value of `Default Root Object` and what does it mean?
- How does CloudFront [Deliver Content](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/HowCloudFrontWorks.html)?

Origins:
- What is an origin?
- What is the value of `Origin Type` and what does it mean?
- What is the origin of the CloudFront distribution?
- What is the bucket name of the distribution bucket?

## Accessing CloudFront
When the distribution is finished, which can take up to 15 minutes, access the distribution by using the domain name in
your browser. There should be no content.

## Distributing content
- Copy the file `html/index.html` to the bucket.
- Access the CloudFront distribution. 
- It can take some time before the content is available and CloudFront can respond with HTTP 307, but after some minutes
the content should become available.

## Cache Invalidation
To update a file, eg `index.html`, the file should first be removed from edge caches. 
Read [Invalidating Files](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/Invalidation.html) to understand the process. 

Invalidate `index.html`:

```text
aws cloudfront create-invalidation --distribution-id <distribution-id> --paths /index.html --profile xtreme-deploy
```

It takes a minute for the cache invalidation to complete. You can view the progress in the AWS Web Console -> CloudFront
-> Invalidations.

Make a change to `index.html` and copy the file to the deployment bucket. After some time it should become available in CloudFront.

## Cleanup

- Delete the stack (can take up to 15 minutes)
